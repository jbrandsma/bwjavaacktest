package test.com;

import java.util.ArrayList;


public class MessageList implements  java.io.Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4018294598322186551L;
	ArrayList<Message> messageList;
	
	public MessageList()
	{
		messageList = new ArrayList<Message>();
	}
	
	public void addMessage(Message inputMessage)
	{
		messageList.add(inputMessage);
	}
	
	public void ack()
	{
		int i;
		for (i=0;i<messageList.size();i++)
		{
			messageList.get(i).ack();
		}
	}
	
}
