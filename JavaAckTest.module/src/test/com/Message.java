package test.com;

import java.util.concurrent.ThreadLocalRandom;

public class Message implements  java.io.Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2357463747779078950L;
	int id;
	
	public Message()
	{
		id = ThreadLocalRandom.current().nextInt(1, 999);
		System.out.println("Created message with id:"+id);
	}
	
	public void ack()
	{
		System.out.println("Acknowledged message with id:"+id);
	}

}
